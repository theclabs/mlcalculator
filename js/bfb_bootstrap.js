/*
 * Copyright 2013 Research In Motion Limited.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// called by the webworks ready event



var app = {
    calculated: false,
	checked:false
}

function initApp() {
    console.log('main init');
    bb.pushScreen('main.html', 'main');
}

function showAbout() {
    var message = "MercadoLibre Calc -  Contacto : federicoagustinarias@gmail.com",
       buttonText = "yeah!",
       toastId,
       onButtonSelected = function () {
          console.log('Button was clicked for toast: ' + toastId);
		  
       },
       onToastDismissed = function () {
          console.log('Toast disappeared: ' + toastId);
		  
       },
       options = {
         buttonText : buttonText,
         dissmissCallback : function () {
          console.log('Toast disappeared: ' + toastId);
		  
       },
         buttonCallback : onButtonSelected,
         timeout : 5000
       };

   toastId = blackberry.ui.toast.show(message, options);  
}

function compartir(){

if (app.checked){

stringtipo = document.getElementById('currency').value;
intpreciounidad = document.getElementById('itemPrice').value;
intunidades = document.getElementById('itemQuantity').value;

showShare(stringtipo, intunidades, intpreciounidad);
  } else { toast("Complete los campos y calcule antes de compartir");
			}
}	

function showShare(stringtipo, intunidades, intpreciounidad){

try{
   var costopub,
    costoxventa,
    costofinal;

check = stringtipo;
		if (check == "Oro Premium") {
            costopub = premiumPublicArg(intpreciounidad, intunidades);
            costoxventa = premiumSellArg(intpreciounidad);
            costofinal = (costoxventa * intunidades);
        }

        if (check == "Oro") {
            costopub = goldPublicArg(intpreciounidad, intunidades);
            costoxventa = goldSellArg(intpreciounidad);
            costofinal = (costoxventa * intunidades);
        }

        if (check == "Plata") {
            costopub = platePublicArg(intpreciounidad, intunidades);
            costoxventa = plateSellArg(intpreciounidad);
            costofinal = (costoxventa * intunidades);
        }

        if (check == "Bronce") {
            costopub = broncePublicArg(intpreciounidad, intunidades);
            costoxventa = bronceSellArg(intpreciounidad);
            costofinal = (costoxventa * intunidades);
        }

var datinga = 'Costos publicacion '+ stringtipo + ' en MercadoLibre para ' + intunidades +  ' unidades a un precio de: $'+ intpreciounidad +  ' por unidad. Costos por publicacion: $'+ costopub + '. Comision por venta x unidad: $'+ costoxventa +'. Con un gasto final de: $' + costofinal + ' al terminar con la venta.';
console.log("datos a compartir "+datinga);

var request = {
		action: 'bb.action.SHARE',
		type: "text/plain",
		data: datinga,
		target_type: ["APPLICATION", "VIEWER", "CARD"]
	};


	blackberry.invoke.card.invokeTargetPicker(request, "Share",
    // success callback
    function() {
        console.log('success');
		toast("Enviado.");
    },

    // error callback
    function(e) {
        console.log('error: ' + e);
		toast("No se pudo compartir, por favor reintente.");
    }
);
}
catch(err)
  {
  console.log('error: ' + err);
		toast("No se pudo compartir, por favor reintente.");
  }


}


// setup window covers, and register with bbm platform
function welcome() {
    // show a welcome message
   // toast("Welcome to the BFB Sample!");

    // setup the window cover (displayed when app is minimized)
    blackberry.ui.cover.setContent(blackberry.ui.cover.TYPE_IMAGE, {
        path: 'local:///cover.png'
    });
    blackberry.ui.cover.updateCover();

    // register with bbm
    bbm.register();
}


// called when access is given by the user to connect w/bbm via bbm.register()
function accessChangedCallback(accessible, status) {
    if (status == "unregistered") {
        // App is unregistered, proceed to register
        registerApp();
    } else if (status == "allowed") { }
    // Listen for other status...
}


// setup the global bbm object so we can call bbm.<function> from where-ever in the app
var bbm = {
    registered: false,

    // registers this application with the blackberry.bbm.platform APIs.
    register: function () {
        blackberry.event.addEventListener('onaccesschanged', function (accessible, status) {

            if (status === 'unregistered') {
                blackberry.bbm.platform.register({
                    uuid: '1735f066-a10e-439b-acc5-8cb93c81fa2d'
                });

            } else if (status === 'allowed') {
                bbm.registered = accessible;
            }

        }, false);
    },

    // update the users personal message in bbm
    updateMessage: function () {

        // dialog callback
        function dialogCallBack(selection) {
            var txt = selection.promptText;
			var txt2 = "Descarga MercadoLibre calc! " + txt;
            blackberry.bbm.platform.self.setPersonalMessage(
			txt2,

			function (accepted) {
			});
        }

        // standard async dialog to get new 'personal message' for bbm
        blackberry.ui.dialog.standardAskAsync("Descarga MercadoLibre Calc! : ", blackberry.ui.dialog.D_PROMPT, dialogCallBack, {
            title: "Comparte esta app!"
        });
    },

    // invite a contact to download your app via bbm
    inviteToDownload: function () {
        blackberry.bbm.platform.users.inviteToDownload();
    }
};




// load the share card
function loadShareCard(title, request) {
    blackberry.invoke.card.invokeTargetPicker(request, title,

	// success
	function () { },

	// error
	function (e) {
	});
}


// display a toast message to the user
function toast(msg) {
    blackberry.ui.toast.show(msg);
}



function Remove(EId) {
    return (EObj = document.getElementById(EId)) ? EObj.parentNode.removeChild(EObj) : false;
}

function calc() {
    var check,
    precio,
    cant,
    costopub,
    costoxventa,
    costofinal;
	
	pre = document.getElementById('itemPrice').value;
    can = document.getElementById('itemQuantity').value;
	
if( pre > 0 && can > 0 )
 { app.checked = true;
    if (app.calculated) {
        console.log("Actualizando...");
        Remove("costopub");
        Remove("costoxventa");
        Remove("costototal");
        app.calculated = false;

    }


    if (!app.calculated) {

        app.calculated = true;
        check = document.getElementById('currency').value;
        precio = document.getElementById('itemPrice').value;
        cant = document.getElementById('itemQuantity').value;

        if (check == "Oro Premium") {
            costopub = premiumPublicArg(precio, cant);
            costoxventa = premiumSellArg(precio);
            costofinal = (costoxventa * cant);


            setDataOnTheFly("costopub", "Costo de publicacion", costopub, "Total");
            setDataOnTheFly("costoxventa", "Comision por venta", costoxventa, "Por Unidad");
            setDataOnTheFly("costototal", "Costo total", costofinal, "Monto total");
        }

        if (check == "Oro") {
            costopub = goldPublicArg(precio, cant);
            costoxventa = goldSellArg(precio);
            costofinal = (costoxventa * cant);

            setDataOnTheFly("costopub", "Costo de publicacion", costopub, "Total");
            setDataOnTheFly("costoxventa", "Comision por venta", costoxventa, "Por Unidad");
            setDataOnTheFly("costototal", "Costo total", costofinal, "Monto total");
        }

        if (check == "Plata") {
            costopub = platePublicArg(precio, cant);
            costoxventa = plateSellArg(precio);
            costofinal = (costoxventa * cant);

            setDataOnTheFly("costopub", "Costo de publicacion", costopub, "Total");
            setDataOnTheFly("costoxventa", "Comision por venta", costoxventa, "Por Unidad");
            setDataOnTheFly("costototal", "Costo total", costofinal, "Monto total");
        }

        if (check == "Bronce") {
            costopub = broncePublicArg(precio, cant);
            costoxventa = bronceSellArg(precio);
            costofinal = (costoxventa * cant);

            setDataOnTheFly('costopub', 'Costo de publicacion', costopub, 'Total');
            setDataOnTheFly('costoxventa', 'Comision por venta', costoxventa, 'Por Unidad');
            setDataOnTheFly('costototal', 'Costo total', costofinal, 'Monto total');
        }
    }

  }	else { toast("Complete correctamente los campos para poder calcular");}
}

function setDataOnTheFly(id, title, value, accent) {
    var listItem;
    var dataList = document.getElementById('lista');
    // Create our list item
    listItem = document.createElement('div');
    listItem.setAttribute('id', id);
    listItem.setAttribute('data-bb-type', 'item');
    listItem.setAttribute('data-bb-title', title);
    listItem.setAttribute('data-bb-accent-text', accent);
    listItem.innerHTML = '$ ' + value;
    dataList.appendItem(listItem);
}


// calcula precios para oro premium arg
function premiumPublicArg(precio, cantidad) {
    var precio;
    var tarifa;
    var temp;

    this.precio = parseFloat(precio).toFixed(2);

    if ((precio * cantidad) < 3000.00) {
        tarifa = 150.00;
        return tarifa;
    }

    if (((precio * cantidad) > 2999.99) && ((precio * cantidad) <= 12999.99)) {
        temp = ((precio / 100) * 5);
        tarifa = (temp * cantidad);
        return tarifa;
    }

    if ((precio * cantidad) >= 13000.00) {
        tarifa = 650.00;
        return tarifa;
    }

}

// calcula precios para ventas oro premium arg X UNIDAD
function premiumSellArg(precio) {
    var precio;
    var tarifa;
    this.precio = parseFloat(precio).toFixed(2);

    if (this.precio < 50.00) {
        tarifa = 3.25;
        return tarifa;
    }

    if ((this.precio >= 50.00) || (this.precio <= 8361.99)) {
        tarifa = ((precio / 100) * 6.5);
        return tarifa;
    }

    if (this.precio >= 8462.00) {
        tarifa = 550.00;
        return tarifa;
    }

}

// calcula precios para oro  arg
function goldPublicArg(precio, cantidad) {
    var precio;
    var tarifa;
    var temp;

    this.precio = parseFloat(precio).toFixed(2);

    if ((precio * cantidad) < 1000.00) {
        tarifa = 30.00;
        return tarifa;
    }

    if (((precio * cantidad) > 999.99) && ((precio * cantidad) <= 12999.99)) {
        temp = ((precio / 100) * 3);
        tarifa = (temp * cantidad);
        return tarifa;
    }

    if ((precio * cantidad) >= 13000.00) {
        tarifa = 390.00;
        return tarifa;
    }

}

// calcula precios para ventas oro  arg X UNIDAD
function goldSellArg(precio) {
    var precio;
    var tarifa;
    this.precio = parseFloat(precio).toFixed(2);

    if (this.precio < 50.00) {
        tarifa = 3.25;
        return tarifa;
    }

    if ((this.precio > 49.99) && (this.precio <= 8361.99)) {
        tarifa = ((precio / 100) * 6.5);
        return tarifa;
    }

    if (this.precio >= 8462.00) {
        tarifa = 550.00;
        return tarifa;
    }

}

// calcula precios para plata  arg
function platePublicArg(precio, cantidad) {
    var precio;
    var tarifa;
    var temp;

    this.precio = parseFloat(precio).toFixed(2);

    if ((precio * cantidad) < 400.00) {
        tarifa = 4.00;
        return tarifa;
    }

    if (((precio * cantidad) > 399.99) && ((precio * cantidad) <= 12999.99)) {
        temp = ((precio / 100) * 1);
        tarifa = (temp * cantidad);
        return tarifa;
    }

    if ((precio * cantidad) >= 13000.00) {
        tarifa = 130.00;
        return tarifa;
    }

}

// calcula precios para ventas plata  arg X UNIDAD
function plateSellArg(precio) {
    var precio;
    var tarifa;
    this.precio = parseFloat(precio).toFixed(2);

    if (this.precio < 50.00) {
        tarifa = 3.25;
        return tarifa;
    }

    if ((this.precio > 49.99) && (this.precio <= 8361.99)) {
        tarifa = ((precio / 100) * 6.5);
        return tarifa;
    }

    if (this.precio >= 8462.00) {
        tarifa = 550.00;
        return tarifa;
    }

}

// calcula precios para bronce arg
function broncePublicArg(precio, cantidad) {
    var precio;
    var tarifa;

    this.precio = parseFloat(precio).toFixed(2);
    tarifa = 0.00;
    return tarifa;

}

// calcula precios para ventas bronce  arg X UNIDAD
function bronceSellArg(precio) {
    var precio;
    var tarifa;
    this.precio = parseFloat(precio).toFixed(2);

    if (this.precio < 50.00) {
        tarifa = 5.00;
        return tarifa;
    }

    if ((this.precio > 49.99) && (this.precio <= 8499.99)) {
        tarifa = ((precio / 100) * 10);
        return tarifa;
    }

    if (this.precio >= 8500.00) {
        tarifa = 850.00;
        return tarifa;
    }

}
